import kotlin.test.assertEquals
import kotlin.test.assertTrue

fun main(){
    var p = Point(1.0, 5.0)
    var p1 = Point(1.0, 2.0)
    assertEquals(p == p1, false);
    assertEquals(p.distanceBetween(p1), 3.0);
    var pSymmetric = Point(-1.0, -5.0);
    p.toSymmetric();
    assertEquals(p, pSymmetric);
    
}