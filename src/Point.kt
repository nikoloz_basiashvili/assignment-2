import kotlin.math.pow
import kotlin.math.sqrt

class Point(private var x: Double, private var y: Double) {

    fun distanceBetween(point: Point): Double{
        return sqrt((point.x - this.x).pow(2.0) + ((point.y - this.y).pow(2.0)))
    }

    fun toSymmetric(): Unit{
        this.x *= -1
        this.y *= -1
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Point

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    override fun toString(): String {
        return "Point(x=$x, y=$y)"
    }

}