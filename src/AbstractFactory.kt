interface AbstractFactory<T>{
    fun create(type:String) : T
}